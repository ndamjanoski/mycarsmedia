package com.goworks.mycarmedia.core.base;

public interface BasePresenter<T> {

    void takeView(T view);

    void dropView();

}
