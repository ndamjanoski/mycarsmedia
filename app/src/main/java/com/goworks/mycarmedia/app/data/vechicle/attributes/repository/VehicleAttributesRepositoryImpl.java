package com.goworks.mycarmedia.app.data.vechicle.attributes.repository;

import com.goworks.mycarmedia.app.domain.vechicle.attributes.service.VehicleAttributesCacheService;
import com.goworks.mycarmedia.app.domain.vechicle.attributes.service.VehicleAttributesRepository;
import com.goworks.mycarmedia.app.domain.vechicle.attributes.service.VehicleAttributesService;
import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created on 2/21/18.
 */

public class VehicleAttributesRepositoryImpl implements VehicleAttributesRepository {

    private final VehicleAttributesService vehicleAttributesService;
    private final VehicleAttributesCacheService vehicleAttributesCacheService;

    @Inject
    public VehicleAttributesRepositoryImpl(VehicleAttributesService vehicleAttributesService,
                                           VehicleAttributesCacheService vehicleAttributesCacheService) {
        this.vehicleAttributesService = vehicleAttributesService;
        this.vehicleAttributesCacheService = vehicleAttributesCacheService;
    }

    @Override
    public rx.Observable<VehicleAttributesResult> getVehicleAttributesInfo() {
        return vehicleAttributesService.pollVehicleInfo()
                .map(new Func1<VehicleAttributesResult, VehicleAttributesResult>() {
                    @Override
                    public VehicleAttributesResult call(VehicleAttributesResult vehicleAttributesResult) {
                        if(vehicleAttributesResult != null){
                            vehicleAttributesCacheService.writeVehicleInfo(vehicleAttributesResult);
                        }
                        return vehicleAttributesResult;
                    }
                })
                .onErrorResumeNext(new Func1<Throwable, Observable<VehicleAttributesResult>>() {
                    @Override
                    public Observable<VehicleAttributesResult> call(Throwable throwable) {
                        return vehicleAttributesCacheService.getVehicleInfo();
                    }
                });
    }
}
