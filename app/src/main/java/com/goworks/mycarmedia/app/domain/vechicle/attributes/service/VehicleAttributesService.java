package com.goworks.mycarmedia.app.domain.vechicle.attributes.service;

import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;

import rx.Observable;

/**
 * Created on 2/20/18.
 */

public interface VehicleAttributesService {
    Observable<VehicleAttributesResult> pollVehicleInfo();
}
