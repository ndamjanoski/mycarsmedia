package com.goworks.mycarmedia.app.data.vechicle.attributes.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created on 2/20/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmissionInfoDto {
    private final EmissionDto gasolineDto;
    private final EmissionDto dieselDto;

    EmissionInfoDto(@JsonProperty("gasoline") EmissionDto gasolineDto,
                    @JsonProperty("diesel") EmissionDto dieselDto) {
        this.gasolineDto = gasolineDto;
        this.dieselDto = dieselDto;
    }

    public EmissionDto getGasolineDto() {
        return gasolineDto;
    }

    public EmissionDto getDieselDto() {
        return dieselDto;
    }
}
