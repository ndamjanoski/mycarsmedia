package com.goworks.mycarmedia.app.domain.vechicle.enums;

import java.util.Locale;

/**
 * Created on 2/21/18.
 */

public enum GearboxType {

    UNKNOWN("unknown"),
    MANUAL("manual"),
    AUTOMATIC("automatic"),
    CVT("cvt"),
    SEMI_AUTOMATIC("semi_automatic"),
    TIPTRONIC("tiptronic"),
    DSG("dsg");

    private final String name;

    GearboxType(String name) {
        this.name = name;
    }


    public String toString() {
        return this.name;
    }

    public static GearboxType fromString(String from) {
        if (from != null) {
            String fromLowerCase = from.toLowerCase(Locale.ENGLISH);
            for (GearboxType gearboxType : values()) {
                if (gearboxType.name.equals(fromLowerCase)) {
                    return gearboxType;
                }
            }
        }
        return UNKNOWN;
    }

}
