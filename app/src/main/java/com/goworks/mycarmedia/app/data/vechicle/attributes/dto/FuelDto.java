package com.goworks.mycarmedia.app.data.vechicle.attributes.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created on 2/20/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FuelDto {

    private final EfficiencyVariablesDto avegrageConsumptionDto;
    private final long tankVolume;
    FuelDto(@JsonProperty("average_consumption") EfficiencyVariablesDto avegrageConsumptionDto,
            @JsonProperty("tank_volume") long tankVolume) {
        this.avegrageConsumptionDto = avegrageConsumptionDto;
        this.tankVolume = tankVolume;
    }

    public EfficiencyVariablesDto getAvegrageConsumptionDto() {
        return avegrageConsumptionDto;
    }

    public long getTankVolume() {
        return tankVolume;
    }
}
