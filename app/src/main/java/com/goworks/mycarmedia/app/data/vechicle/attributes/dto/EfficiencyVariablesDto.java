package com.goworks.mycarmedia.app.data.vechicle.attributes.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Created on 2/20/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EfficiencyVariablesDto {
    private final BigDecimal mixed;
    private final BigDecimal rural;
    private final BigDecimal urban;

    EfficiencyVariablesDto(@JsonProperty("mixed") BigDecimal mixed, @JsonProperty("rural") BigDecimal rural,
                           @JsonProperty("urban") BigDecimal urban) {
        this.mixed = mixed;
        this.rural = rural;
        this.urban = urban;
    }

    @JsonProperty("mixed")
    public BigDecimal getMixed() {
        return mixed;
    }

    @JsonProperty("rural")
    public BigDecimal getRural() {
        return rural;
    }

    @JsonProperty("urban")
    public BigDecimal getUrban() {
        return urban;
    }


}
