package com.goworks.mycarmedia.app.data.vechicle.attributes.mapper;

import com.goworks.mycarmedia.app.data.vechicle.attributes.dto.EfficiencyVariablesDto;
import com.goworks.mycarmedia.app.data.vechicle.attributes.dto.FuelDto;
import com.goworks.mycarmedia.app.data.vechicle.attributes.dto.VehicleAttributesDto;
import com.goworks.mycarmedia.app.domain.vechicle.enums.FuelType;
import com.goworks.mycarmedia.app.domain.vechicle.enums.GearboxType;
import com.goworks.mycarmedia.app.entity.vehicle.Emission;
import com.goworks.mycarmedia.app.entity.vehicle.Fuel;
import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 2/20/18.
 */

public class VehicleAttributesMapperImpl implements VehicleAttributesMapper {

    @Override
    public VehicleAttributesResult convert(VehicleAttributesDto vechicleAttributesDto) {
        if(vechicleAttributesDto.getVin() == null || vechicleAttributesDto.getRegno() == null){
            return null;
        }else {
            VehicleAttributesResult vehicleAttributesResult = map(vechicleAttributesDto);
            return vehicleAttributesResult;
        }
    }

    private VehicleAttributesResult map(VehicleAttributesDto vechicleAttributesDto) {
        String regNo = vechicleAttributesDto.getRegno();
        String vin = vechicleAttributesDto.getVin();
        String brand = vechicleAttributesDto.getBrand();
        int year = vechicleAttributesDto.getYear();
        List<FuelType> fuelTypes = new LinkedList<>();
        List<Fuel> fuels = new LinkedList<>();
        List<Emission> emissions = new LinkedList<>();
        for(String fuelStr : vechicleAttributesDto.getFuelTypes()){
            FuelType fuelType = FuelType.fromString(fuelStr);
            fuelTypes.add(fuelType);
            switch (fuelType){
                case GASOLINE:
                    fuels.add(getFuel(vechicleAttributesDto.getFuelInfoDto().getGasolineDto(), fuelType));
                    emissions.add(getEmission(vechicleAttributesDto.getEmissionInfoDto().getGasolineDto().getEmissions(), fuelType));
                    break;
                case DIESEL:
                    fuels.add(getFuel(vechicleAttributesDto.getFuelInfoDto().getDieselDto(), fuelType));
                    emissions.add(getEmission(vechicleAttributesDto.getEmissionInfoDto().getDieselDto().getEmissions(), fuelType));
                    break;
            }
        }

        GearboxType gearboxType = GearboxType.fromString(vechicleAttributesDto.getGearboxType());
        Date timestamp = vechicleAttributesDto.getTimestamp();

        return new VehicleAttributesResult(regNo, vin, brand, year, fuelTypes, fuels, emissions, gearboxType, timestamp);
    }

    private Emission getEmission(EfficiencyVariablesDto emissions, FuelType fuelType) {
        return new Emission(emissions.getMixed(), emissions.getRural(), emissions.getUrban(), fuelType);
    }

    private Fuel getFuel(FuelDto gasolineDto, FuelType fuelType) {
        BigDecimal urban = gasolineDto.getAvegrageConsumptionDto().getUrban();
        BigDecimal mixed = gasolineDto.getAvegrageConsumptionDto().getMixed();
        BigDecimal rural = gasolineDto.getAvegrageConsumptionDto().getRural();
        long tankVol = gasolineDto.getTankVolume();
        return new Fuel(mixed, rural, urban, fuelType, tankVol);
    }
}
