package com.goworks.mycarmedia.app.data.vechicle.attributes.service;

import com.goworks.mycarmedia.app.data.vechicle.attributes.dto.VehicleAttributesDto;
import com.goworks.mycarmedia.app.data.vechicle.attributes.mapper.VehicleAttributesMapper;
import com.goworks.mycarmedia.app.domain.vechicle.attributes.service.VehicleAttributesService;
import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;
import com.goworks.mycarmedia.core.utilities.rx.SchedulerProvider;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;


/**
 * Created on 2/20/18.
 */

public class VehicleAttributesServiceImpl implements VehicleAttributesService {

    private final VehicleAttributesMapper vechicleAttributesMapper;
    private final VehicleAttributesPollService attributesPollService;
    private final SchedulerProvider schedulerProvider;
    @Inject
    public VehicleAttributesServiceImpl(VehicleAttributesMapper vechicleAttributesMapper,
                                                    VehicleAttributesPollService attributesPollService, SchedulerProvider schedulerProvider) {
        this.vechicleAttributesMapper = vechicleAttributesMapper;
        this.attributesPollService = attributesPollService;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public Observable<VehicleAttributesResult> pollVehicleInfo() {
        return attributesPollService.pollAttributes()
                .map(new Func1<Response<VehicleAttributesDto>, VehicleAttributesResult>() {
                    @Override
                    public VehicleAttributesResult call(Response<VehicleAttributesDto> vechicleAttributesDtoResponse) {
                        VehicleAttributesResult vechicleAttributesResult = null;
                        if(vechicleAttributesDtoResponse.code() == 200){
                            vechicleAttributesResult = vechicleAttributesMapper.convert(vechicleAttributesDtoResponse.body());
                        }
                        return vechicleAttributesResult;
                    }
                })
                .subscribeOn(schedulerProvider.getIo())
                .observeOn(schedulerProvider.getMain());
    }
}
