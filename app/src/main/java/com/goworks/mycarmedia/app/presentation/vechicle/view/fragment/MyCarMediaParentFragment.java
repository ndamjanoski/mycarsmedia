package com.goworks.mycarmedia.app.presentation.vechicle.view.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.goworks.mycarmedia.core.fragment.base.FragmentBase;

/**
 * Created on 2/20/18.
 */

public class MyCarMediaParentFragment extends FragmentBase {

    protected void addChildFragment(Fragment fragment, int viewGroupId, String tag) {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.add(viewGroupId, fragment, tag);
        ft.commit();
    }

    protected boolean hasChildFragmentWithTag(String tag) {
        return getChildFragmentManager().findFragmentByTag(tag) != null;
    }
}
