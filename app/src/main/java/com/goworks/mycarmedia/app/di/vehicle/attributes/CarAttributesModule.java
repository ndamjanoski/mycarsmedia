package com.goworks.mycarmedia.app.di.vehicle.attributes;

import com.goworks.mycarmedia.app.data.vechicle.attributes.mapper.VehicleAttributesMapper;
import com.goworks.mycarmedia.app.data.vechicle.attributes.mapper.VehicleAttributesMapperImpl;
import com.goworks.mycarmedia.app.data.vechicle.attributes.repository.VehicleAttributesRepositoryImpl;
import com.goworks.mycarmedia.app.data.vechicle.attributes.service.VehicleAttributesPollService;
import com.goworks.mycarmedia.app.data.vechicle.attributes.service.VehicleAttributesServiceCacheImpl;
import com.goworks.mycarmedia.app.data.vechicle.attributes.service.VehicleAttributesServiceImpl;
import com.goworks.mycarmedia.app.di.common.ActivityScoped;
import com.goworks.mycarmedia.app.di.common.FragmentScoped;
import com.goworks.mycarmedia.app.domain.vechicle.attributes.service.VehicleAttributesCacheService;
import com.goworks.mycarmedia.app.domain.vechicle.attributes.service.VehicleAttributesRepository;
import com.goworks.mycarmedia.app.domain.vechicle.attributes.service.VehicleAttributesService;
import com.goworks.mycarmedia.app.presentation.vechicle.attributes.fragment.MyCarAttributesFragment;
import com.goworks.mycarmedia.app.presentation.vechicle.attributes.fragment.VehicleAttributesParentFragment;
import com.goworks.mycarmedia.app.presentation.vechicle.attributes.presenter.CarAttributesContract;
import com.goworks.mycarmedia.app.presentation.vechicle.attributes.presenter.VehicleAttributesPresenterImpl;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import retrofit2.Retrofit;

/**
 * Created on 2/20/18.
 */

@Module
public abstract class CarAttributesModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract VehicleAttributesParentFragment vehicleAttributesParentFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract MyCarAttributesFragment myCarAttributesFragment();

    @FragmentScoped
    @Binds abstract CarAttributesContract.CarAttributesPresenter carAttributesPresenter(VehicleAttributesPresenterImpl carAttributesPresenter);

    @Binds
    @ActivityScoped
    abstract VehicleAttributesService provideVehicleAttributesService(VehicleAttributesServiceImpl vechicleAttributesService);

    @Binds
    @ActivityScoped
    abstract VehicleAttributesCacheService provideVehicleAttributesCacheService(VehicleAttributesServiceCacheImpl vehicleAttributesServiceCache);

    @Binds
    @ActivityScoped
    abstract VehicleAttributesRepository provideVehicleAttributesRepository(VehicleAttributesRepositoryImpl vehicleAttributesRepository);

    @Provides
    @ActivityScoped
    static VehicleAttributesPollService provideService(Retrofit baseService){
        return baseService.create(VehicleAttributesPollService.class);
    }

    @Provides
    @ActivityScoped
    static VehicleAttributesMapper provideVechicleAttributesMapper(){
        return new VehicleAttributesMapperImpl();
    }
}
