package com.goworks.mycarmedia.app.entity.vehicle.cache;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Index;

/**
 * Created on 2/22/18.
 */

@Entity(indexes = {
        @Index(value = "vin", unique = true)
})
public class VehicleAttribute {


    private String regNo;
    @Id
    private String vin;
    private String brand;
    private int year;
    private Date timestamp;
    private String gearboxType;
    
    @Generated(hash = 1321476565)
    public VehicleAttribute(String regNo, String vin, String brand, int year,
            Date timestamp, String gearboxType) {
        this.regNo = regNo;
        this.vin = vin;
        this.brand = brand;
        this.year = year;
        this.timestamp = timestamp;
        this.gearboxType = gearboxType;
    }
    @Generated(hash = 1297463541)
    public VehicleAttribute() {
    }
    public String getRegNo() {
        return this.regNo;
    }
    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }
    public String getVin() {
        return this.vin;
    }
    public void setVin(String vin) {
        this.vin = vin;
    }
    public String getBrand() {
        return this.brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public int getYear() {
        return this.year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public Date getTimestamp() {
        return this.timestamp;
    }
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
    public String getGearboxType() {
        return this.gearboxType;
    }
    public void setGearboxType(String gearboxType) {
        this.gearboxType = gearboxType;
    }

}
