package com.goworks.mycarmedia.app.entity.vehicle.cache;

import org.greenrobot.greendao.annotation.Entity;

import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Index;

/**
 * Created on 2/22/18.
 */

@Entity(indexes = {
        @Index(value = "vin", unique = true)
})
public class FuelDb {

    private double mixed;
    private double rural;
    private double urban;
    private String fuelType;
    private int tankVolume;
    private String vin;
    @Generated(hash = 2145569660)
    public FuelDb(double mixed, double rural, double urban, String fuelType,
            int tankVolume, String vin) {
        this.mixed = mixed;
        this.rural = rural;
        this.urban = urban;
        this.fuelType = fuelType;
        this.tankVolume = tankVolume;
        this.vin = vin;
    }
    @Generated(hash = 638593356)
    public FuelDb() {
    }
    public double getMixed() {
        return this.mixed;
    }
    public void setMixed(double mixed) {
        this.mixed = mixed;
    }
    public double getRural() {
        return this.rural;
    }
    public void setRural(double rural) {
        this.rural = rural;
    }
    public double getUrban() {
        return this.urban;
    }
    public void setUrban(double urban) {
        this.urban = urban;
    }
    public String getFuelType() {
        return this.fuelType;
    }
    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }
    public int getTankVolume() {
        return this.tankVolume;
    }
    public void setTankVolume(int tankVolume) {
        this.tankVolume = tankVolume;
    }
    public String getVin() {
        return this.vin;
    }
    public void setVin(String vin) {
        this.vin = vin;
    }
}
