package com.goworks.mycarmedia.app.entity.vehicle;

import android.os.Parcel;
import android.os.Parcelable;

import com.goworks.mycarmedia.app.domain.vechicle.enums.FuelType;

import java.math.BigDecimal;

/**
 * Created on 2/21/18.
 */

public class Fuel extends Efficiency implements Parcelable {

    private final long tankVolume;
    public Fuel(BigDecimal mixed, BigDecimal rural, BigDecimal urban, FuelType fuelType, long tankVolume) {
        super(mixed, rural, urban, fuelType);
        this.tankVolume = tankVolume;
    }

    public long getTankVolume() {
        return tankVolume;
    }


    public Fuel(Parcel in) {
        super(in);
        this.tankVolume = in.readLong();
    }


    public static final Creator<Fuel> CREATOR = new Creator<Fuel>() {
        @Override
        public Fuel createFromParcel(Parcel in) {
            return new Fuel(in);
        }

        @Override
        public Fuel[] newArray(int size) {
            return new Fuel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeLong(tankVolume);
    }
}
