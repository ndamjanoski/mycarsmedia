package com.goworks.mycarmedia.app.entity.vehicle.attributes;

import android.os.Parcel;
import android.os.Parcelable;

import com.goworks.mycarmedia.app.domain.vechicle.enums.FuelType;
import com.goworks.mycarmedia.app.domain.vechicle.enums.GearboxType;
import com.goworks.mycarmedia.app.entity.vehicle.Emission;
import com.goworks.mycarmedia.app.entity.vehicle.Fuel;

import java.util.Date;
import java.util.List;

/**
 * Created on 2/20/18.
 */

public class VehicleAttributesResult implements Parcelable {

    private final String regNo;
    private final String vin;
    private final String brand;
    private final int year;
    private final List<FuelType> fuelTypes;
    private final GearboxType gearboxType;
    private final Date timestamp;
    private List<Fuel> fuels;
    private List<Emission> emissions;
    private boolean isCache;

    public VehicleAttributesResult(String regNo, String vin, String brand, int year, List<FuelType> fuelTypes,
                                   List<Fuel> fuels, List<Emission> emissions, GearboxType gearboxType, Date timestamp) {
        this.regNo = regNo;
        this.vin = vin;
        this.brand = brand;
        this.year = year;
        this.fuelTypes = fuelTypes;
        this.fuels = fuels;
        this.emissions = emissions;
        this.gearboxType = gearboxType;
        this.timestamp = timestamp;
    }

    public String getRegNo() {
        return regNo;
    }

    public String getVin() {
        return vin;
    }

    public String getBrand() {
        return brand;
    }

    public int getYear() {
        return year;
    }

    public List<FuelType> getFuelTypes() {
        return fuelTypes;
    }

    public GearboxType getGearboxType() {
        return gearboxType;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public List<Fuel> getFuels() {
        return fuels;
    }

    public List<Emission> getEmissions() {
        return emissions;
    }

    public void setCache(boolean cache) {
        isCache = cache;
    }

    public boolean isCache() {
        return isCache;
    }

    protected VehicleAttributesResult(Parcel in) {
        regNo = in.readString();
        vin = in.readString();
        brand = in.readString();
        year = in.readInt();
        fuelTypes = in.readArrayList(getClass().getClassLoader());
        gearboxType = GearboxType.fromString(in.readString());
        timestamp = (Date) in.readSerializable();
        in.readTypedList(fuels, Fuel.CREATOR);
        in.readTypedList(emissions, Emission.CREATOR);
    }

    public static final Creator<VehicleAttributesResult> CREATOR = new Creator<VehicleAttributesResult>() {
        @Override
        public VehicleAttributesResult createFromParcel(Parcel in) {
            return new VehicleAttributesResult(in);
        }

        @Override
        public VehicleAttributesResult[] newArray(int size) {
            return new VehicleAttributesResult[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(regNo);
        dest.writeString(vin);
        dest.writeString(brand);
        dest.writeInt(year);
        dest.writeList(fuelTypes);
        dest.writeString(gearboxType.toString());
        dest.writeSerializable(timestamp);
        dest.writeTypedList(fuels);
        dest.writeTypedList(emissions);
    }
}
