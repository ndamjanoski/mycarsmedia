package com.goworks.mycarmedia.app.data.vechicle.attributes.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VehicleAttributesDto {

    private final String regno;
    private final String vin;
    private final String brand;
    private final int year;
    private final String gearboxType;
    private final List<String> fuelTypes;
    private final FuelInfoDto fuelInfoDto;
    private final EmissionInfoDto emissionInfoDto;
    private final Date timestamp;

    public VehicleAttributesDto(@JsonProperty("regno") String regno, @JsonProperty("vin") String vin,
                                @JsonProperty("brand") String brand, @JsonProperty("year") int year,
                                @JsonProperty("gearbox_type") String gearboxType,
                                @JsonProperty("fuel_types") List<String> fuelTypes,
                                @JsonProperty("fuel") FuelInfoDto fuelInfoDto,
                                @JsonProperty("emission") EmissionInfoDto emissionInfoDto,
                                @JsonProperty("timestamp") Date timestamp) {
        this.regno = regno;
        this.vin = vin;
        this.brand = brand;
        this.year = year;
        this.gearboxType = gearboxType;
        this.fuelTypes = fuelTypes;
        this.fuelInfoDto = fuelInfoDto;
        this.emissionInfoDto = emissionInfoDto;
        this.timestamp = timestamp;
    }

    public String getRegno() {
        return regno;
    }

    public String getVin() {
        return vin;
    }

    public String getBrand() {
        return brand;
    }

    public int getYear() {
        return year;
    }


    public String getGearboxType() {
        return gearboxType;
    }

    public List<String> getFuelTypes() {
        return fuelTypes;
    }

    public FuelInfoDto getFuelInfoDto() {
        return fuelInfoDto;
    }

    public EmissionInfoDto getEmissionInfoDto() {
        return emissionInfoDto;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
