package com.goworks.mycarmedia.app.presentation.vechicle.attributes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goworks.mycarmedia.R;
import com.goworks.mycarmedia.app.presentation.vechicle.view.fragment.MyCarMediaParentFragment;

import javax.inject.Inject;

/**
 * Created on 2/20/18.
 */

public class VehicleAttributesParentFragment extends MyCarMediaParentFragment {

    public static final String TAG = "MyCarMediaParentFragment";
    private DrawerLayout drawerLayout;
    private View contentHolderView;

    public static VehicleAttributesParentFragment newInstance(){
        VehicleAttributesParentFragment carMediaParentFragment = new VehicleAttributesParentFragment();
        Bundle args = new Bundle();
        carMediaParentFragment.setArguments(args);
        return carMediaParentFragment;
    }

    @Inject
    public VehicleAttributesParentFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_mycar_parent, container, false);
        drawerLayout = root.findViewById(R.id.drawer);
        contentHolderView = root.findViewById(R.id.myCarContentHolder);
        initFragments();
        getChildFragmentManager().executePendingTransactions();
        return root;
    }

    private void initFragments() {
        MyCarAttributesFragment attributesFragment;
        if (!hasChildFragmentWithTag(MyCarAttributesFragment.TAG)) {
            attributesFragment = MyCarAttributesFragment.newInstance();
            addChildFragment(attributesFragment, R.id.activityContent, MyCarAttributesFragment.TAG);
        }
    }
}
