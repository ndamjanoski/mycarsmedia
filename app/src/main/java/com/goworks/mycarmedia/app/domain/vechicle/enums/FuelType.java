package com.goworks.mycarmedia.app.domain.vechicle.enums;

import java.util.Locale;

/**
 * Created on 2/21/18.
 */

public enum FuelType {

    UNKNOWN("unknown"),
    GASOLINE("gasoline"),
    DIESEL("diesel"),
    ELECTRIC("electric"),
    HIBRID("hibrid");

    private final String name;

    FuelType(String name) {
        this.name = name;
    }


    public String toString() {
        return this.name;
    }

    public static FuelType fromString(String from) {
        if (from != null) {
            String fromLowerCase = from.toLowerCase(Locale.ENGLISH);
            for (FuelType fuelType : values()) {
                if (fuelType.name.equals(fromLowerCase)) {
                    return fuelType;
                }
            }
        }
        return UNKNOWN;
    }
}
