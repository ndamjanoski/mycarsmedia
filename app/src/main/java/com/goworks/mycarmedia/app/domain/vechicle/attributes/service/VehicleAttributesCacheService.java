package com.goworks.mycarmedia.app.domain.vechicle.attributes.service;

import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;

import rx.Observable;

/**
 * Created on 2/22/18.
 */

public interface VehicleAttributesCacheService {
    Observable<VehicleAttributesResult> getVehicleInfo();


    void writeVehicleInfo(VehicleAttributesResult vehicleAttributesResult);
}
