package com.goworks.mycarmedia.app.presentation.vechicle.attributes.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v17.leanback.widget.ClassPresenterSelector;
import android.support.v17.leanback.widget.PresenterSelector;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.goworks.mycarmedia.R;
import com.goworks.mycarmedia.app.presentation.vechicle.attributes.listcell.AttributeCell;
import com.goworks.mycarmedia.app.presentation.vechicle.attributes.pojo.AttributeItem;
import com.goworks.mycarmedia.app.presentation.vechicle.attributes.presenter.CarAttributesContract;
import com.goworks.mycarmedia.core.adapter.CmArrayObjectAdapter;
import com.goworks.mycarmedia.core.adapter.RecyclerViewAdapter;
import com.goworks.mycarmedia.core.fragment.base.FragmentBase;
import com.goworks.mycarmedia.core.utilities.CmTextUtil;

import java.util.List;

import javax.inject.Inject;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;

/**
 * Created on 2/20/18.
 */

public class MyCarAttributesFragment extends FragmentBase implements CarAttributesContract.CarAttributesView {

    public static final String TAG = "MyCarAttributesFragment";
    @Inject
    CarAttributesContract.CarAttributesPresenter presenter;
    private RecyclerView list;
    private CmArrayObjectAdapter adapterItems;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private int subTitleColor;
    private ProgressBar progressBar;
    private View listWrapper;
    private Button errorButton;
    private View errorText;


    public static MyCarAttributesFragment newInstance() {
        MyCarAttributesFragment myCarAttributesFragment = new MyCarAttributesFragment();
        Bundle args = new Bundle();
        myCarAttributesFragment.setArguments(args);
        return myCarAttributesFragment;
    }


    @Inject
    public MyCarAttributesFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        subTitleColor = getResources().getColor(R.color.white_70);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_car_attributes, null);
        toolbar = view.findViewById(R.id.activityToolbar);
        toolbarTitle = view.findViewById(R.id.toolBarTitle);
        progressBar = view.findViewById(R.id.progressBar);
        list = view.findViewById(R.id.list);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(getActivity(), 1, false));
        list.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        list.setItemAnimator(new SlideInLeftAnimator());
        adapterItems = new CmArrayObjectAdapter();
        RecyclerViewAdapter originalAdapter = new RecyclerViewAdapter(adapterItems, getListPresenter());
        list.setAdapter(originalAdapter);
        listWrapper = view.findViewById(R.id.listWrapper);
        errorText = view.findViewById(R.id.errorText);
        errorButton = view.findViewById(R.id.errorButton);
        errorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.visualizeData();
                updateStateForProgress();
            }
        });
        setToolbar();
        presenter.takeView(this);
        return view;
    }

    /**
     * To avoid listWrapper flickering we handle state
     * only for retry button and sync
     */
    private void updateStateForProgress() {
        errorText.setVisibility(View.GONE);
        errorButton.setVisibility(View.GONE);
        listWrapper.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void setToolbar() {
        toolbar.inflateMenu(R.menu.menu_carattributes);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId() == R.id.menu_sync){
                    presenter.visualizeData();
                    updateStateForProgress();
                }
                return false;
            }
        });
    }

    private PresenterSelector getListPresenter() {
        ClassPresenterSelector selector = new ClassPresenterSelector();
        selector.addClassPresenter(AttributeItem.class, new AttributeCell());
        return selector;
    }

    @Override
    public void updateToolbarText(String firstLine, String secondLine) {
        toolbarTitle.setText(CmTextUtil.createToolbarTwoLineTitle(firstLine, secondLine, subTitleColor),
                TextView.BufferType.SPANNABLE);
    }

    @Override
    public void updateState(boolean isError, boolean isCache) {
        if(isError){
            listWrapper.setVisibility(View.GONE);
            errorText.setVisibility(View.VISIBLE);
            errorButton.setVisibility(View.VISIBLE);
        }else {
            if(listWrapper.getVisibility() != View.VISIBLE){
                listWrapper.setVisibility(View.VISIBLE);
            }
            if(errorText.getVisibility() != View.GONE){
                errorText.setVisibility(View.GONE);
            }
            if(errorButton.getVisibility() != View.GONE){
                errorButton.setVisibility(View.GONE);
            }
        }
        progressBar.setVisibility(View.GONE);
        if(isCache){
            Snackbar.make(getView(), R.string.cached_info_message, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showData(List<Object> objectList) {
        adapterItems.setData(objectList);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }
}
