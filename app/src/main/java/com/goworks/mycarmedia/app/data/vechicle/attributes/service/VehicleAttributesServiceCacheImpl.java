package com.goworks.mycarmedia.app.data.vechicle.attributes.service;

import android.support.annotation.Nullable;

import com.goworks.mycarmedia.app.domain.vechicle.attributes.service.VehicleAttributesCacheService;
import com.goworks.mycarmedia.app.domain.vechicle.enums.FuelType;
import com.goworks.mycarmedia.app.domain.vechicle.enums.GearboxType;
import com.goworks.mycarmedia.app.entity.vehicle.Emission;
import com.goworks.mycarmedia.app.entity.vehicle.Fuel;
import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;
import com.goworks.mycarmedia.app.entity.vehicle.cache.DaoSession;
import com.goworks.mycarmedia.app.entity.vehicle.cache.EmissionDb;
import com.goworks.mycarmedia.app.entity.vehicle.cache.EmissionDbDao;
import com.goworks.mycarmedia.app.entity.vehicle.cache.FuelDb;
import com.goworks.mycarmedia.app.entity.vehicle.cache.FuelDbDao;
import com.goworks.mycarmedia.app.entity.vehicle.cache.FuelTypeDb;
import com.goworks.mycarmedia.app.entity.vehicle.cache.FuelTypeDbDao;
import com.goworks.mycarmedia.app.entity.vehicle.cache.VehicleAttribute;
import com.goworks.mycarmedia.app.entity.vehicle.cache.VehicleAttributeDao;
import com.goworks.mycarmedia.core.utilities.rx.SchedulerProvider;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func0;

/**
 * Created on 2/22/18.
 */

public class VehicleAttributesServiceCacheImpl implements VehicleAttributesCacheService {


    private final DaoSession daoSession;
    private final SchedulerProvider schedulerProvider;

    @Inject
    public VehicleAttributesServiceCacheImpl(DaoSession daoSession, SchedulerProvider schedulerProvider) {
        this.daoSession = daoSession;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public Observable<VehicleAttributesResult> getVehicleInfo() {
        return Observable.fromCallable(new Func0<VehicleAttributesResult>() {
            @Override
            public VehicleAttributesResult call() {
                VehicleAttributesResult vehicleAttributesResult = getVehicleAttributesResult();

                return vehicleAttributesResult;
            }
        }).subscribeOn(schedulerProvider.getComputation())
                .observeOn(schedulerProvider.getMain());
    }

    @Nullable
    private VehicleAttributesResult getVehicleAttributesResult() {
        VehicleAttributeDao vehicleDao = daoSession.getVehicleAttributeDao();

        List<VehicleAttribute> attList = vehicleDao.queryBuilder().list();
        if(attList == null || attList.size() == 0) return null;

        VehicleAttribute attribute = attList.get(0);
        String vin = attribute.getVin();
        String regNo = attribute.getRegNo();
        String brand = attribute.getBrand();
        int year = attribute.getYear();

        FuelTypeDbDao fuelTypeDao = daoSession.getFuelTypeDbDao();
        FuelDbDao fuelDao = daoSession.getFuelDbDao();
        EmissionDbDao emissionDao = daoSession.getEmissionDbDao();
        List<FuelDb> fuelDbList = fuelDao.queryBuilder().where(FuelDbDao.Properties.Vin.eq(vin)).list();
        List<EmissionDb> emissionsDbList = emissionDao.queryBuilder().where(EmissionDbDao.Properties.Vin.eq(vin)).list();
        List<FuelTypeDb> fueltypesDbList = fuelTypeDao.queryBuilder().where(FuelTypeDbDao.Properties.Vin.eq(vin)).list();
        List<FuelType> fuelTypes = new LinkedList<>();
        List<Fuel> fuels = new LinkedList<>();
        List<Emission> emissions = new LinkedList<>();

        for(FuelTypeDb fuelTypeDb : fueltypesDbList){
            fuelTypes.add(FuelType.fromString(fuelTypeDb.getName()));
        }

        for(FuelDb fuelDb : fuelDbList){
            Fuel fuel = new Fuel(BigDecimal.valueOf(fuelDb.getMixed()), BigDecimal.valueOf(fuelDb.getRural()),
                    BigDecimal.valueOf(fuelDb.getUrban()), FuelType.fromString(fuelDb.getFuelType()),
                    fuelDb.getTankVolume());
            fuels.add(fuel);
        }

        for(EmissionDb emissionDb : emissionsDbList){
            Emission emission = new Emission(BigDecimal.valueOf(emissionDb.getMixed()), BigDecimal.valueOf(emissionDb.getRural()),
                    BigDecimal.valueOf(emissionDb.getUrban()), FuelType.fromString(emissionDb.getFuelType()));
            emissions.add(emission);
        }

        GearboxType gearboxType = GearboxType.fromString(attribute.getGearboxType());
        Date timestamp = attribute.getTimestamp();

        VehicleAttributesResult vehicleAttributesResult = new VehicleAttributesResult(regNo, vin, brand, year,
                fuelTypes, fuels, emissions, gearboxType, timestamp);
        vehicleAttributesResult.setCache(true);
        return vehicleAttributesResult;
    }

    @Override
    public void writeVehicleInfo(VehicleAttributesResult vehicleAttributesResult) {
        FuelTypeDbDao fuelTypeDao = daoSession.getFuelTypeDbDao();
        String vin = vehicleAttributesResult.getVin();
        for(FuelType fuelType : vehicleAttributesResult.getFuelTypes()){
            FuelTypeDb fuelTypeDb = new FuelTypeDb(fuelType.toString(), vin);
            fuelTypeDao.insertOrReplace(fuelTypeDb);
        }
        FuelDbDao fuelDao = daoSession.getFuelDbDao();
        for(Fuel fuel : vehicleAttributesResult.getFuels()){
            FuelDb fuelDb = new FuelDb(fuel.getMixed().doubleValue(), fuel.getRural().doubleValue(),
                    fuel.getUrban().doubleValue(), fuel.getFuelType().toString(),
                    (int) fuel.getTankVolume(), vin);
            fuelDao.insertOrReplace(fuelDb);
        }
        EmissionDbDao emissionDao = daoSession.getEmissionDbDao();
        for(Emission emission : vehicleAttributesResult.getEmissions()){
            EmissionDb emissionDb = new EmissionDb(emission.getMixed() != null ? emission.getMixed().doubleValue() : 0,
                    emission.getRural() != null ? emission.getRural().doubleValue() : 0,
                    emission.getUrban() != null ? emission.getUrban().doubleValue() : 0,
                    emission.getFuelType().toString(), vin);
            emissionDao.insertOrReplace(emissionDb);
        }
        VehicleAttributeDao vehicleDao = daoSession.getVehicleAttributeDao();
        VehicleAttribute vehicleAttribute = new VehicleAttribute(vehicleAttributesResult.getRegNo(), vin, vehicleAttributesResult.getBrand(),
                vehicleAttributesResult.getYear(), vehicleAttributesResult.getTimestamp(), vehicleAttributesResult.getGearboxType().toString());
        vehicleDao.insertOrReplace(vehicleAttribute);
    }
}
