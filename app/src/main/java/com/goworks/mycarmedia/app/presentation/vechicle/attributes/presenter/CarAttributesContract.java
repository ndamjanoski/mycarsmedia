package com.goworks.mycarmedia.app.presentation.vechicle.attributes.presenter;

import com.goworks.mycarmedia.core.base.BasePresenter;
import com.goworks.mycarmedia.core.base.BaseView;

import java.util.List;

/**
 * Created on 2/20/18.
 */

public interface CarAttributesContract {
    interface CarAttributesView extends BaseView<CarAttributesPresenter> {

        void showData(List<Object> objectList);

        void updateToolbarText(String firstLine, String secondLine);

        void updateState(boolean isError, boolean isCache);
    }

    interface CarAttributesPresenter extends BasePresenter<CarAttributesView> {
        void visualizeData();
    }
}
