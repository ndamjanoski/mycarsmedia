package com.goworks.mycarmedia.app.presentation.vechicle.attributes.listcell;

import android.graphics.drawable.ColorDrawable;
import android.support.v17.leanback.widget.Presenter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goworks.mycarmedia.R;
import com.goworks.mycarmedia.app.presentation.vechicle.attributes.pojo.AttributeItem;
import com.goworks.mycarmedia.core.lists.ChildClickPresenter;

/**
 * Created on 2/21/18.
 */

public class AttributeCell extends ChildClickPresenter {

    public class AttributeCellViewHolder extends Presenter.ViewHolder implements AttrubuteViewHolder{

        private final TextView textDesc;
        private final TextView textResult;
        private final LinearLayout view;
        private final int margin;
        public static final int NO_MARGIN = 0;
        public AttributeCellViewHolder(View view) {
            super(view);
            this.view = (LinearLayout) view;
            textDesc = view.findViewById(R.id.description);
            textResult = view.findViewById(R.id.result);
            margin = (int) view.getContext().getResources().getDimension(R.dimen.cell_margin);
        }

        @Override
        public void bindCellViewData(String desc, String result, int backgroundColor, boolean isTitle) {
            textDesc.setText(desc);
            textResult.setText(result);
            if(((ColorDrawable) view.getBackground()).getColor() != backgroundColor) {
                view.setBackgroundColor(backgroundColor);
            }
            setMargins(isTitle);
        }

        private void setMargins(boolean isTitle) {
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view.getLayoutParams();
            if(isTitle){
                params.setMargins(NO_MARGIN, NO_MARGIN, NO_MARGIN, NO_MARGIN);
                params.setMarginStart(NO_MARGIN);
                params.setMarginEnd(NO_MARGIN);
            }else if(params.getMarginEnd() == NO_MARGIN){
                params.setMargins(margin, margin, margin, margin);
                params.setMarginStart(margin);
                params.setMarginEnd(margin);
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new AttributeCellViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_vehicle_attribute,
                parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        final AttributeItem attributeItem = (AttributeItem) item;
        AttributeCellViewHolder cellViewHolder = (AttributeCellViewHolder) viewHolder;
        cellViewHolder.bindCellViewData(attributeItem.getDescription(), attributeItem.getResult(),
                attributeItem.getBackgroundColor(), attributeItem.isTitle());
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }
}
