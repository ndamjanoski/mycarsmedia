package com.goworks.mycarmedia.app.data.vechicle.attributes.service;

import com.goworks.mycarmedia.app.data.vechicle.attributes.dto.VehicleAttributesDto;

import retrofit2.Response;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created on 2/20/18.
 */

public interface VehicleAttributesPollService {

    @GET("8f2400621822407a868e3d6524c767ab/raw/5382ff4d9692727861b16f0c3ad8d89e6a6377ef/vehicle-attributes.json")
    Observable<Response<VehicleAttributesDto>> pollAttributes();
}
