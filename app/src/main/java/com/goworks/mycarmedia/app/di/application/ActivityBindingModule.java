package com.goworks.mycarmedia.app.di.application;



import com.goworks.mycarmedia.app.di.common.ActivityScoped;
import com.goworks.mycarmedia.app.di.vehicle.VehicleCommonModule;
import com.goworks.mycarmedia.app.di.vehicle.attributes.CarAttributesModule;
import com.goworks.mycarmedia.app.presentation.vechicle.view.activity.MyCarMediaActivity2;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = {CarAttributesModule.class, VehicleCommonModule.class})
    abstract MyCarMediaActivity2 myCarMediaActivity();
}
