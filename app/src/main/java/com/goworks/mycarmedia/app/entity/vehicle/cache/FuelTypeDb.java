package com.goworks.mycarmedia.app.entity.vehicle.cache;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Index;

/**
 * Created on 2/22/18.
 */

@Entity(indexes = {
        @Index(value = "vin", unique = true)
})
public class FuelTypeDb {

    private String name;
    private String vin;

    @Generated(hash = 1812623229)
    public FuelTypeDb() {
    }
    @Generated(hash = 756376514)
    public FuelTypeDb(String name, String vin) {
        this.name = name;
        this.vin = vin;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getVin() {
        return this.vin;
    }
    public void setVin(String vin) {
        this.vin = vin;
    }
}
