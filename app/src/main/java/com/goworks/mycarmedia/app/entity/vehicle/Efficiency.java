package com.goworks.mycarmedia.app.entity.vehicle;

import android.os.Parcel;
import android.os.Parcelable;

import com.goworks.mycarmedia.app.domain.vechicle.enums.FuelType;

import java.math.BigDecimal;

/**
 * Created on 2/21/18.
 */

public class Efficiency implements Parcelable {
    private final BigDecimal mixed;
    private final BigDecimal rural;
    private final BigDecimal urban;
    private final FuelType fuelType;

    public Efficiency(BigDecimal mixed, BigDecimal rural, BigDecimal urban, FuelType fuelType) {
        this.mixed = mixed;
        this.rural = rural;
        this.urban = urban;
        this.fuelType = fuelType;
    }


    public BigDecimal getMixed() {
        return mixed;
    }

    public BigDecimal getRural() {
        return rural;
    }

    public BigDecimal getUrban() {
        return urban;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    protected Efficiency(Parcel in) {
        this.mixed = BigDecimal.valueOf(in.readDouble());
        this.rural = BigDecimal.valueOf(in.readDouble());
        this.urban = BigDecimal.valueOf(in.readDouble());
        this.fuelType = FuelType.fromString(in.readString());

    }

    public static final Creator<Efficiency> CREATOR = new Creator<Efficiency>() {
        @Override
        public Efficiency createFromParcel(Parcel in) {
            return new Efficiency(in);
        }

        @Override
        public Efficiency[] newArray(int size) {
            return new Efficiency[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(mixed.doubleValue());
        dest.writeDouble(rural.doubleValue());
        dest.writeDouble(urban.doubleValue());
        dest.writeString(fuelType.toString());
    }
}
