package com.goworks.mycarmedia.app.domain.vechicle.attributes.service;

import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;

import rx.Observable;

/**
 * Created on 2/21/18.
 */

public interface VehicleAttributesRepository {
    Observable<VehicleAttributesResult> getVehicleAttributesInfo();
}
