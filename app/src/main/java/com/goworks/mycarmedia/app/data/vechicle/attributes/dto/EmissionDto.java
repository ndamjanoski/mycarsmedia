package com.goworks.mycarmedia.app.data.vechicle.attributes.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created on 2/20/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmissionDto {
    private final EfficiencyVariablesDto emissions;

    EmissionDto(@JsonProperty("co2") EfficiencyVariablesDto emissions) {
        this.emissions = emissions;
    }

    public EfficiencyVariablesDto getEmissions() {
        return emissions;
    }
}
