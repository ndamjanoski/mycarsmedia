package com.goworks.mycarmedia.app.entity.vehicle.cache;

import org.greenrobot.greendao.annotation.Entity;

import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Index;

/**
 * Created on 2/22/18.
 */
@Entity(indexes = {
        @Index(value = "vin", unique = true)
})
public class EmissionDb {


    private double mixed;
    private double rural;
    private double urban;
    private String fuelType;
    private String vin;

    @Generated(hash = 309163845)
    public EmissionDb() {
    }
    @Generated(hash = 1827651966)
    public EmissionDb(double mixed, double rural, double urban, String fuelType,
            String vin) {
        this.mixed = mixed;
        this.rural = rural;
        this.urban = urban;
        this.fuelType = fuelType;
        this.vin = vin;
    }
    public double getMixed() {
        return this.mixed;
    }
    public void setMixed(double mixed) {
        this.mixed = mixed;
    }
    public double getRural() {
        return this.rural;
    }
    public void setRural(double rural) {
        this.rural = rural;
    }
    public double getUrban() {
        return this.urban;
    }
    public void setUrban(double urban) {
        this.urban = urban;
    }
    public String getFuelType() {
        return this.fuelType;
    }
    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }
    public String getVin() {
        return this.vin;
    }
    public void setVin(String vin) {
        this.vin = vin;
    }
}
