package com.goworks.mycarmedia.app.di.vehicle;

import android.content.Context;

import com.goworks.mycarmedia.BuildConfig;
import com.goworks.mycarmedia.app.di.common.ActivityScoped;
import com.goworks.mycarmedia.app.entity.vehicle.cache.DaoMaster;
import com.goworks.mycarmedia.app.entity.vehicle.cache.DaoSession;
import com.goworks.mycarmedia.core.utilities.rx.SchedulerProvider;
import com.goworks.mycarmedia.core.utilities.rx.SchedulerProviderImpl;

import org.greenrobot.greendao.database.Database;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created on 2/20/18.
 */

@Module
public abstract class VehicleCommonModule {

    @Provides
    @ActivityScoped
    static Retrofit provideServiceRetrofit() {
        OkHttpClient httpClient = new OkHttpClient()
                .newBuilder()
                .build();
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient).build();
    }

    @Provides
    @ActivityScoped
    static SchedulerProvider provideScheduler(){
        return new SchedulerProviderImpl();
    }

    @Provides
    @ActivityScoped
    static DaoSession provideDaoSession(Context context){
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(context, "vehicles.db");
        Database database = devOpenHelper.getWritableDb();
        return new DaoMaster(database).newSession();
    }
}
