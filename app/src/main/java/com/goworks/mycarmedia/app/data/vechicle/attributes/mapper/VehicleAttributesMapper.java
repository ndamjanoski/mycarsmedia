package com.goworks.mycarmedia.app.data.vechicle.attributes.mapper;

import com.goworks.mycarmedia.app.data.vechicle.attributes.dto.VehicleAttributesDto;
import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;

/**
 * Created on 2/20/18.
 */

public interface VehicleAttributesMapper {
    VehicleAttributesResult convert(VehicleAttributesDto vehicleAttributesDto);
}
