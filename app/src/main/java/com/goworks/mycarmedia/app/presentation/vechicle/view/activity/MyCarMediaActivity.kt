//package com.goworks.mycarmedia.app.presentation.vechicle.view.activity
//
//import android.os.Bundle
//import com.goworks.mycarmedia.R
//import com.goworks.mycarmedia.app.presentation.vechicle.attributes.fragment.VehicleAttributesParentFragment
//import com.goworks.mycarmedia.core.activity.base.ActivityBase
//
//class MyCarMediaActivity : ActivityBase() {
//
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//        var carAttributesParentFragment = supportFragmentManager.findFragmentByTag(VehicleAttributesParentFragment.TAG)
//                as? VehicleAttributesParentFragment
//        if(carAttributesParentFragment == null){
//            carAttributesParentFragment = VehicleAttributesParentFragment.newInstance()
//            val ft = supportFragmentManager.beginTransaction()
//            ft.add(R.id.activityContent, carAttributesParentFragment, VehicleAttributesParentFragment.TAG)
//            ft.commit()
//            supportFragmentManager.executePendingTransactions()
//        }
//    }
//}
