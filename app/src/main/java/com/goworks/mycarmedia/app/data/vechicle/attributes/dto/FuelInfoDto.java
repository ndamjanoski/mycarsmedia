package com.goworks.mycarmedia.app.data.vechicle.attributes.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created on 2/20/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FuelInfoDto {
    private final FuelDto gasolineDto;
    private final FuelDto dieselDto;


    FuelInfoDto(@JsonProperty("gasoline") FuelDto gasolineDto, @JsonProperty("diesel") FuelDto dieselDto) {
        this.gasolineDto = gasolineDto;
        this.dieselDto = dieselDto;
    }

    public FuelDto getGasolineDto() {
        return gasolineDto;
    }

    public FuelDto getDieselDto() {
        return dieselDto;
    }
}
