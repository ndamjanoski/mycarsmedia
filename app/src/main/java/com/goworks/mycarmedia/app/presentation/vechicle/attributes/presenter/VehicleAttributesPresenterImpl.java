package com.goworks.mycarmedia.app.presentation.vechicle.attributes.presenter;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.goworks.mycarmedia.R;
import com.goworks.mycarmedia.app.domain.vechicle.attributes.service.VehicleAttributesRepository;
import com.goworks.mycarmedia.app.domain.vechicle.enums.FuelType;
import com.goworks.mycarmedia.app.entity.vehicle.Emission;
import com.goworks.mycarmedia.app.entity.vehicle.Fuel;
import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;
import com.goworks.mycarmedia.app.presentation.vechicle.attributes.pojo.AttributeItem;
import com.goworks.mycarmedia.core.utilities.CmTextUtil;
import com.goworks.mycarmedia.core.utilities.EspressoIdlingResource;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;

/**
 * Created on 2/20/18.
 */

public class VehicleAttributesPresenterImpl implements CarAttributesContract.CarAttributesPresenter {


    private CarAttributesContract.CarAttributesView view;
    private final VehicleAttributesRepository vechicleAttributesService;
    private Context context;
    private int colorDef;
    private int colorDark;
    private int colorMed;
    private String shortLiters;
    private String gramPerMeter;

    @Inject
    public VehicleAttributesPresenterImpl(VehicleAttributesRepository vehicleAttributesService, Context context) {
        this.vechicleAttributesService = vehicleAttributesService;
        this.context = context;
    }


    @Override
    public void takeView(CarAttributesContract.CarAttributesView view) {
        this.view = view;
        colorDef = ContextCompat.getColor(getContxt(), R.color.cell_card_background);
        colorMed = ContextCompat.getColor(getContxt(), R.color.cell_card_background_medium);
        colorDark = ContextCompat.getColor(getContxt(), R.color.cell_card_background_dark);
        shortLiters = getContxt().getString(R.string.short_liters);
        gramPerMeter = getContxt().getString(R.string.gram_per_meter);
        visualizeData();
    }

    public void visualizeData() {

        EspressoIdlingResource.increment();

        vechicleAttributesService.getVehicleAttributesInfo()
                .subscribe(new Subscriber<VehicleAttributesResult>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
                            EspressoIdlingResource.decrement(); // Set app as idle.
                        }
                    }

                    @Override
                    public void onNext(VehicleAttributesResult vehicleAttributesResult) {
                        if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
                            EspressoIdlingResource.decrement(); // Set app as idle.
                        }
                        if(vehicleAttributesResult == null){
                            view.updateState(true, false);
                            return;
                        }
                        view.updateState(false, vehicleAttributesResult.isCache());
                        List<Object> items = prepareItems(vehicleAttributesResult);
                        visualizeData(vehicleAttributesResult, items);
                    }
                });
    }

    private void visualizeData(VehicleAttributesResult vehicleAttributesResult, List<Object> items) {
        view.updateToolbarText(formatPrimaryText(vehicleAttributesResult.getBrand(),
                vehicleAttributesResult.getRegNo()),
                formatSecondaryText(vehicleAttributesResult.getTimestamp()));
        view.showData(items);
    }

    protected List<Object> prepareItems(VehicleAttributesResult vehicleAttributesResult) {
        List<Object> items = new LinkedList<>();
        items.add(getItem(getContxt().getString(R.string.vin_number), vehicleAttributesResult.getVin()));
        items.add(getItem(getContxt().getString(R.string.reg_no), vehicleAttributesResult.getRegNo()));

        items.add(getItem(getContxt().getString(R.string.manufactured), String.valueOf(vehicleAttributesResult.getYear())));
        items.add(getItem(getContxt().getString(R.string.gearbox),
                CmTextUtil.capitalizeFirstLetter(vehicleAttributesResult.getGearboxType().toString())));

        items.add(getItem(getContxt().getString(R.string.consumption), null, true, colorDark));
        for(FuelType fuelType : vehicleAttributesResult.getFuelTypes()){
            items.add(getItem(CmTextUtil.capitalizeFirstLetter(fuelType.toString()), null, true, colorMed));
            Fuel fuel = getFuel(vehicleAttributesResult.getFuels(), fuelType);
            if(fuel.getUrban() != null && fuel.getUrban().doubleValue() != 0){
                items.add(getItem(getContxt().getString(R.string.urban), convertToDefLitersValue(fuel.getUrban())));
            }
            if(fuel.getRural() != null && fuel.getRural().doubleValue() != 0){
                items.add(getItem(getContxt().getString(R.string.rural), convertToDefLitersValue(fuel.getRural())));
            }
            if(fuel.getMixed() != null && fuel.getMixed().doubleValue() != 0){
                items.add(getItem(getContxt().getString(R.string.mixed), convertToDefLitersValue(fuel.getMixed())));
            }
            if(fuel.getTankVolume() > 0){
                items.add(getItem(getContxt().getString(R.string.tank_volume), String.valueOf(fuel.getTankVolume())));
            }
        }

        items.add(getItem(getContxt().getString(R.string.emissions), null, true, colorDark));
        for(FuelType fuelType : vehicleAttributesResult.getFuelTypes()){
            items.add(getItem(CmTextUtil.capitalizeFirstLetter(fuelType.toString()), null, true, colorMed));
            Emission emission = getEmission(vehicleAttributesResult.getEmissions(), fuelType);
            if(emission.getUrban() != null && emission.getUrban().doubleValue() != 0){
                items.add(getItem(getContxt().getString(R.string.urban), convertToDefGramMeterValue(emission.getUrban())));
            }
            if(emission.getRural() != null && emission.getRural().doubleValue() != 0){
                items.add(getItem(getContxt().getString(R.string.rural), convertToDefGramMeterValue(emission.getRural())));
            }
            if(emission.getMixed() != null && emission.getMixed().doubleValue() != 0){
                items.add(getItem(getContxt().getString(R.string.mixed), convertToDefGramMeterValue(emission.getMixed())));
            }
        }

        return items;
    }

    private String convertToDefGramMeterValue(BigDecimal urban) {
        double res = urban.doubleValue() * 1000;
        return String.valueOf(res) + gramPerMeter;
    }

    private String convertToDefLitersValue(BigDecimal urban) {
        double thousandM = 1000;
        double hundredKm = 100;
        double res = urban.doubleValue() * thousandM * hundredKm;
        res = Math.round(res * 10000d) / 10000d;
        return String.valueOf(res) + shortLiters;
    }

    protected Emission getEmission(List<Emission> emissions, FuelType fuelType) {
        for (Emission emission : emissions){
            if(emission.getFuelType().equals(fuelType)){
                return emission;
            }
        }
        return null;
    }

    protected Fuel getFuel(List<Fuel> fuels, FuelType fuelType) {
        for(Fuel fuel : fuels){
            if(fuel.getFuelType().equals(fuelType)){
                return fuel;
            }
        }
        return null;
    }

    private AttributeItem getItem(String desc, String res) {
        return getItem(desc, res, false, colorDef);
    }

    private AttributeItem getItem(String desc, String res, boolean isTitle, int backgroundColor) {
        return new AttributeItem(desc, res, isTitle, backgroundColor);
    }

    private String formatPrimaryText(String brand, String regNo) {
        return CmTextUtil.capitalizeFirstLetter(brand) + CmTextUtil.COMMON_SEPARATOR + regNo;
    }

    private String formatSecondaryText(Date timestamp) {
        String date = formatDate(timestamp);
        return context.getString(R.string.updated) + CmTextUtil.COMMON_COLON_SEPARATOR + date;
    }

    private String formatDate(Date timestamp) {
        SimpleDateFormat dateFormatTo = new SimpleDateFormat("HH:mm  dd MMM yyyy");
        return dateFormatTo.format(timestamp);
    }

    @Override
    public void dropView() {
        view = null;
        context = null;
    }

    public Context getContxt() {
        return context;
    }
}
