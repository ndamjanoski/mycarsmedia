package com.goworks.mycarmedia.app.presentation.vechicle.attributes.pojo;

import com.goworks.mycarmedia.core.utilities.CmTextUtil;

/**
 * Created on 2/21/18.
 */

public class AttributeItem {
    private String description;
    private final String result;
    private final boolean isTitle;
    private final int backgroundColor;
    public AttributeItem(String description, String result, boolean isTitle, int backgroundColor) {
        if(isTitle){
            this.description = description;
        }else {
            this.description = description + CmTextUtil.COMMON_COLON_SEPARATOR;
        }
        this.result = result;
        this.isTitle = isTitle;
        this.backgroundColor = backgroundColor;
    }

    public String getDescription() {
        return description;
    }

    public String getResult() {
        return result;
    }

    public boolean isTitle() {
        return isTitle;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }
}
