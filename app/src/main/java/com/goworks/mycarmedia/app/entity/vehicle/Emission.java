package com.goworks.mycarmedia.app.entity.vehicle;

import android.os.Parcel;
import android.os.Parcelable;

import com.goworks.mycarmedia.app.domain.vechicle.enums.FuelType;

import java.math.BigDecimal;

/**
 * Created on 2/21/18.
 */

public class Emission extends Efficiency implements Parcelable {

    public Emission(BigDecimal mixed, BigDecimal rural, BigDecimal urban, FuelType fuelType) {
        super(mixed, rural, urban, fuelType);
    }

    public Emission(Parcel in) {
        super(in);
    }


    public static final Creator<Emission> CREATOR = new Creator<Emission>() {
        @Override
        public Emission createFromParcel(Parcel in) {
            return new Emission(in);
        }

        @Override
        public Emission[] newArray(int size) {
            return new Emission[size];
        }
    };
}
