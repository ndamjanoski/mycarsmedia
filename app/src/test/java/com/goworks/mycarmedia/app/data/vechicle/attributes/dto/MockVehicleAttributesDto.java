package com.goworks.mycarmedia.app.data.vechicle.attributes.dto;

import com.goworks.mycarmedia.app.domain.vechicle.enums.FuelType;
import com.goworks.mycarmedia.app.entity.vechicle.attributes.MockVehicleAttributeValues;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 2/26/18.
 */

public class MockVehicleAttributesDto extends MockVehicleAttributeValues {

    private static VehicleAttributesDto INSTANCE;

    public static VehicleAttributesDto getInstance(){
        if(INSTANCE == null){
            INSTANCE = getMockVehicleAttributesDto();
        }
        return INSTANCE;
    }

    private static VehicleAttributesDto getMockVehicleAttributesDto() {
        List<String> fuelTypes = new LinkedList<>();
        fuelTypes.add(FuelType.GASOLINE.toString());
        EfficiencyVariablesDto efficiencyVariablesDto = new EfficiencyVariablesDto(BigDecimal.valueOf(fuelUrban), BigDecimal.valueOf(fuelRural),
                BigDecimal.valueOf(fuelMixed));
        FuelDto gasolineFuelDto = new FuelDto(efficiencyVariablesDto, tankVol);
        FuelInfoDto fuelInfoDto = new FuelInfoDto(gasolineFuelDto, null);
        EfficiencyVariablesDto emissionEfficiencyVariablesDto = new EfficiencyVariablesDto(BigDecimal.valueOf(emissionUrban),
                null, null);
        EmissionDto gasolineEmissionDto = new EmissionDto(emissionEfficiencyVariablesDto);
        EmissionInfoDto emissionInfoDto = new EmissionInfoDto(gasolineEmissionDto, null);
        VehicleAttributesDto vehicleAttributesDto = new VehicleAttributesDto(regNo, vin, brand, year, gearboxType.toString(),
                fuelTypes, fuelInfoDto, emissionInfoDto, timestamp);
        return vehicleAttributesDto;
    }


}
