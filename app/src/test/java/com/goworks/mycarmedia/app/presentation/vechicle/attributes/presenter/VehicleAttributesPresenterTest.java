package com.goworks.mycarmedia.app.presentation.vechicle.attributes.presenter;

import android.os.Build;
import android.test.mock.MockContext;

import com.goworks.mycarmedia.app.domain.vechicle.attributes.service.VehicleAttributesRepository;
import com.goworks.mycarmedia.app.entity.vechicle.attributes.MockVehicleAttributesResult;
import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;

import rx.Observable;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created on 2/21/18.
 */

public class VehicleAttributesPresenterTest {

    @Mock
    private CarAttributesContract.CarAttributesView carAttributesView;

    private VehicleAttributesPresenterImpl presenter;

    private VehicleAttributesResult vehicleAttributesResult;

    @Mock
    private VehicleAttributesRepository vehicleAttributesRepository;

    private int itemsSize;

    @Before
    public void setUpPresenter(){

        MockitoAnnotations.initMocks(this);

        initStaticBuldVersion();

        MockContext mockContext = mock(MockContext.class);
        doReturn("Sample")
                .when(mockContext)
                .getString(any(Integer.class));
        doReturn(10000)
                .when(mockContext).getColor(any(Integer.class));

        itemsSize = MockVehicleAttributesResult.ITEMS_SIZE;
        vehicleAttributesResult = MockVehicleAttributesResult.getInstance();

        presenter = new VehicleAttributesPresenterImpl(vehicleAttributesRepository, mockContext);
    }

    @After
    public void SetUpAfter(){
        vehicleAttributesResult.setCache(false);
    }

    private void initStaticBuldVersion() {
        try {

            Field sdkIntField = Build.VERSION.class.getDeclaredField("SDK_INT");
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(sdkIntField, sdkIntField.getModifiers() & ~Modifier.FINAL);
            sdkIntField.set(Build.VERSION.SDK_INT, 26);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void prepareListItemsTest(){

        List<Object> items = presenter.prepareItems(vehicleAttributesResult);

        assertTrue(items.size() == itemsSize);
    }

    @Test
    public void showSuccessVehicleAttributesResultTest(){

        when(vehicleAttributesRepository.getVehicleAttributesInfo())
                .thenReturn(Observable.just(vehicleAttributesResult));

        presenter.takeView(carAttributesView);

        ArgumentCaptor<List> itemsResultArgumentCaptor = ArgumentCaptor.forClass(List.class);

        verify(carAttributesView).showData(itemsResultArgumentCaptor.capture());

        assertTrue(itemsResultArgumentCaptor.getValue().size() == itemsSize);
    }

    @Test
    public void showNoResultFailedVehicleAttributesTest(){

        when(vehicleAttributesRepository.getVehicleAttributesInfo())
                .thenReturn(Observable.<VehicleAttributesResult>just(null));

        presenter.takeView(carAttributesView);

        ArgumentCaptor<Boolean> isErrorArgumentCaptor = ArgumentCaptor.forClass(Boolean.class);
        ArgumentCaptor<Boolean> isCacheArgumentCaptor = ArgumentCaptor.forClass(Boolean.class);

        verify(carAttributesView).updateState(isErrorArgumentCaptor.capture(), isCacheArgumentCaptor.capture());

        assertTrue(isErrorArgumentCaptor.getValue());
        assertFalse(isCacheArgumentCaptor.getValue());
    }

    @Test
    public void showCachedResultVehicleAttributesResultTest(){

        vehicleAttributesResult.setCache(true);
        when(vehicleAttributesRepository.getVehicleAttributesInfo())
                .thenReturn(Observable.<VehicleAttributesResult>just(vehicleAttributesResult));

        presenter.takeView(carAttributesView);

        ArgumentCaptor<Boolean> isErrorArgumentCaptor = ArgumentCaptor.forClass(Boolean.class);
        ArgumentCaptor<Boolean> isCacheArgumentCaptor = ArgumentCaptor.forClass(Boolean.class);

        verify(carAttributesView).updateState(isErrorArgumentCaptor.capture(), isCacheArgumentCaptor.capture());

        assertFalse(isErrorArgumentCaptor.getValue());
        assertTrue(isCacheArgumentCaptor.getValue());

        ArgumentCaptor<List> itemsResultArgumentCaptor = ArgumentCaptor.forClass(List.class);

        verify(carAttributesView).showData(itemsResultArgumentCaptor.capture());

        assertTrue(itemsResultArgumentCaptor.getValue().size() == itemsSize);

    }
}
