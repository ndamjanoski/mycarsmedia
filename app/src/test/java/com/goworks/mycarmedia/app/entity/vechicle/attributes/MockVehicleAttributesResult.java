package com.goworks.mycarmedia.app.entity.vechicle.attributes;

import com.goworks.mycarmedia.app.domain.vechicle.enums.FuelType;
import com.goworks.mycarmedia.app.entity.vehicle.Emission;
import com.goworks.mycarmedia.app.entity.vehicle.Fuel;
import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 2/26/18.
 */

public class MockVehicleAttributesResult extends MockVehicleAttributeValues {


    private static VehicleAttributesResult INSTANCE;
    public static final int ITEMS_SIZE = 13;

    public static VehicleAttributesResult getInstance(){
        if(INSTANCE == null){
            INSTANCE = getMockVehicleAttributes();
        }
        return INSTANCE;
    }


    private static VehicleAttributesResult getMockVehicleAttributes() {
        List<FuelType> fuelTypes = new LinkedList<>();
        fuelTypes.add(FuelType.GASOLINE);
        List<Fuel> fuels = new LinkedList<>();
        List<Emission> emissions = new LinkedList<>();
        for(FuelType fuelType : fuelTypes){
            switch (fuelType){
                case GASOLINE:
                    fuels.add(new Fuel(BigDecimal.valueOf(fuelUrban), BigDecimal.valueOf(fuelRural),
                            BigDecimal.valueOf(fuelMixed), fuelType, tankVol));
                    emissions.add(new Emission(BigDecimal.valueOf(emissionUrban), null, null,  fuelType));
                    break;
                case DIESEL:
                    fuels.add(new Fuel(BigDecimal.valueOf(dieselUrban), BigDecimal.valueOf(dieselRural),
                            BigDecimal.valueOf(dieselMixed), fuelType, tankVol));
                    emissions.add(new Emission(BigDecimal.valueOf(emissionUrban), null, null,  fuelType));
                    break;
            }
        }
        return new VehicleAttributesResult(regNo, vin, brand, year, fuelTypes, fuels, emissions, gearboxType, timestamp);
    }

}
