package com.goworks.mycarmedia.app.data.vechicle.attributes;

import com.goworks.mycarmedia.app.data.vechicle.attributes.service.VehicleAttributesMapperTest;
import com.goworks.mycarmedia.app.presentation.vechicle.attributes.presenter.VehicleAttributesPresenterTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created on 2/26/18.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        VehicleAttributesPresenterTest.class,
        VehicleAttributesMapperTest.class
})
public class VehicleAttributesTestSuite {
}
