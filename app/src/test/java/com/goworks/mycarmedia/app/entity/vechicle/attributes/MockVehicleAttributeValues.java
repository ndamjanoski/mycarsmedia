package com.goworks.mycarmedia.app.entity.vechicle.attributes;

import com.goworks.mycarmedia.app.domain.vechicle.enums.GearboxType;

import java.util.Date;

/**
 * Created on 2/26/18.
 */

public class MockVehicleAttributeValues {

    protected static String regNo = "wur816";
    protected static String vin = "tmbga61z852094863";
    protected static String brand = "volvo";
    protected static int year = 2005;
    protected static double fuelUrban = 0.000073;
    protected static double fuelRural = 0.000058;
    protected static double fuelMixed = 0.000099;
    protected static double dieselUrban = 0.000073;
    protected static double dieselRural = 0.000058;
    protected static double dieselMixed = 0.000099;
    protected static int tankVol = 200;
    protected static double emissionUrban = 0.000175;
    protected static double emissionRural = 0;
    protected static double emissionMixed = 0;
    protected static Date timestamp = new Date(System.currentTimeMillis());
    protected static GearboxType gearboxType = GearboxType.MANUAL;

}
