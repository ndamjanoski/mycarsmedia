package com.goworks.mycarmedia.app.data.vechicle.attributes.service;

import com.goworks.mycarmedia.app.data.vechicle.attributes.dto.MockVehicleAttributesDto;
import com.goworks.mycarmedia.app.data.vechicle.attributes.dto.VehicleAttributesDto;
import com.goworks.mycarmedia.app.data.vechicle.attributes.mapper.VehicleAttributesMapper;
import com.goworks.mycarmedia.app.data.vechicle.attributes.mapper.VehicleAttributesMapperImpl;
import com.goworks.mycarmedia.app.domain.vechicle.enums.FuelType;
import com.goworks.mycarmedia.app.entity.vechicle.attributes.MockVehicleAttributesResult;
import com.goworks.mycarmedia.app.entity.vehicle.Emission;
import com.goworks.mycarmedia.app.entity.vehicle.Fuel;
import com.goworks.mycarmedia.app.entity.vehicle.attributes.VehicleAttributesResult;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created on 2/26/18.
 */

public class VehicleAttributesMapperTest {



    private VehicleAttributesMapper vehicleAttributesMapper;

    private VehicleAttributesDto vehicleAttributesDto;

    private VehicleAttributesResult vehicleAttributesResult;

    @Before
    public void setUpMapper(){

        vehicleAttributesDto = MockVehicleAttributesDto.getInstance();
        vehicleAttributesResult = MockVehicleAttributesResult.getInstance();
        vehicleAttributesMapper = new VehicleAttributesMapperImpl();
    }

    @Test
    public void convertVehicleAttributesDtoToResultTest(){

        VehicleAttributesResult vehicleAttributesResultConverted = vehicleAttributesMapper.convert(vehicleAttributesDto);

        boolean isConverted = checkIsConverted(vehicleAttributesResultConverted);
        assertTrue(isConverted);

    }

    private boolean checkIsConverted(VehicleAttributesResult vehicleAttributesResultConverted) {
        int index = 0;
        boolean isEqual = true;
        for(Emission emission : vehicleAttributesResultConverted.getEmissions()){
            Emission emissionCached = vehicleAttributesResult.getEmissions().get(index);
            if(emission.getUrban() != null) {
                isEqual &= emission.getUrban().equals(emissionCached.getUrban());
            }
            if(emission.getRural() != null) {
                isEqual &= emission.getRural().equals(emissionCached.getRural());
            }
            if(emission.getMixed() != null) {
                isEqual &= emission.getMixed().equals(emissionCached.getMixed());
            }
            index++;
        }

        index = 0;
        for(Fuel fuel : vehicleAttributesResultConverted.getFuels()){
            Fuel fuelCached = vehicleAttributesResult.getFuels().get(index);
            if(fuel.getUrban() != null){
                isEqual &= fuel.getUrban().equals(fuelCached.getUrban());
            }
            if(fuel.getRural() != null) {
                isEqual &= fuel.getRural().equals(fuelCached.getRural());
            }
            if(fuel.getMixed() != null) {
                isEqual &= fuel.getMixed().equals(fuelCached.getMixed());
            }
            isEqual &= fuel.getTankVolume() == fuelCached.getTankVolume();
            index++;
        }

        index = 0;
        for(FuelType fuelType : vehicleAttributesResultConverted.getFuelTypes()) {
            FuelType fuelTypeCached = vehicleAttributesResult.getFuelTypes().get(index);
            isEqual &= fuelType.equals(fuelTypeCached);
            index++;
        }
        isEqual &= vehicleAttributesResultConverted.isCache() == vehicleAttributesResult.isCache();
        isEqual &= vehicleAttributesResultConverted.getRegNo().equals(vehicleAttributesResult.getRegNo());
        isEqual &= vehicleAttributesResultConverted.getVin().equals(vehicleAttributesResult.getVin());
        isEqual &= vehicleAttributesResultConverted.getBrand().equals(vehicleAttributesResult.getBrand());
        isEqual &= vehicleAttributesResultConverted.getYear() == vehicleAttributesResult.getYear();
        return isEqual;
    }
}
