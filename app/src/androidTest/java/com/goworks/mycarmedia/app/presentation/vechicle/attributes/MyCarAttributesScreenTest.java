package com.goworks.mycarmedia.app.presentation.vechicle.attributes;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.goworks.mycarmedia.R;
import com.goworks.mycarmedia.app.presentation.vechicle.view.activity.MyCarMediaActivity2;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.core.IsNot.not;

/**
 * Created on 2/25/18.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MyCarAttributesScreenTest {

    private static String MY_CAR_ATTRIBUTES_TITLE = "Volvo wur816";

    @Rule
    public ActivityTestRule<MyCarMediaActivity2> dayViewActivityActivityTestRule = new ActivityTestRule<MyCarMediaActivity2>(
            MyCarMediaActivity2.class, true, false);


    public void startActivity(){
        dayViewActivityActivityTestRule.launchActivity(null);
    }

    @Test
    public void openVehicleAttributesFragmentAndCheckTitleList(){
        startActivity();

        onView(withId(R.id.toolBarTitle)).check(matches(withText(startsWith(MY_CAR_ATTRIBUTES_TITLE))));
    }


}
